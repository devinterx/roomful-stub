var MyPlugin = {
    Trigger: function(eventName, json)
    {
        window.trigger(Pointer_stringify(eventName), Pointer_stringify(json));
    },
	
	TriggerBytes: function(eventName, array, size)
    {		
		window.trigger(Pointer_stringify(eventName), HEAPU8.buffer.slice(array, array + size));
    }
}

mergeInto(LibraryManager.library, MyPlugin);
