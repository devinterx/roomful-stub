﻿using System;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Text;

public class Test : MonoBehaviour
{

    [DllImport("__Internal")]
    private static extern void Trigger(string eventName, string json);

    [DllImport("__Internal")]
    private static extern void TriggerBytes(string eventName, byte[] bytes, int size);

    // Use this for initialization
    void Start()
    {
        //Application.ExternalCall("console.log", "Hello from Unity!");
        //Debug.Log("Hello");
        Trigger("unity-YEAH", "{\"message\":\"Hello World from Unity to Unity\"}");

        byte[] bytes = Encoding.UTF8.GetBytes("This string in bytes, Hello");
        TriggerBytes("unity-bytes", bytes, bytes.Length);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Answer(string json)
    {
        Debug.Log(json);
    }

    public void BytesMessage(string base64EncodedData)
    {
        Debug.Log(base64EncodedData);
        string s = Encoding.UTF8.GetString(Convert.FromBase64String(base64EncodedData)); ;
        Debug.Log(s);
    }
}

